package com.sachin;

import java.util.Arrays;
import java.util.List;

import static  org.junit.Assert.*;
import org.junit.Test;


public class FindLargestInListTest {
	
	@Test
	public void getLargest(){
		List<Integer> integers = Arrays.asList(1,5,7,8,3);
		assertSame(8, new FindLargestInList().getLargest(integers));
	}

}
