package com.sachin;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.sachin.linkedlist.LinkedList;

import junit.framework.Assert;

public class CommonTest {
	
	@Test
	public void testHello(){
		com.sachin.Test test = new com.sachin.Test();
		assertSame("Hello", test.hello());
	
	}

	@Test
	public void testFindLargestInList(){
		List<Integer> integers = Arrays.asList(1,5,7,8,3);
		assertSame(8, new FindLargestInList().getLargest(integers));
	}
	
	@Test
	public void arraySortWithNaturalArraySortTest(){
		ArraySort arraySort = new ArraySort();
		assertArrayEquals(new Integer[] {1, 2,3,4,5},arraySort.naturalArraySort(new Integer[] {4,3,5,1,2}));
	}
	
	@Test
	public void arraySortWithCustomReverseArraySortTest(){
		ArraySort arraySort = new ArraySort();
		assertArrayEquals(new Integer[] {5,4,3,2,1},arraySort.customReverseArraySort(new Integer[] {4,3,5,1,2}));
	}
	
	
	@Test
	public void bubbleSortWithForLoopTest(){
		
		BubbleSort bubbleSort = new BubbleSort();
		assertArrayEquals(new Integer[] {1,2,3,4,5}, bubbleSort.bubbleSortByForWhileLoop(Arrays.asList(1,5,7,8,3)).toArray());
	}
	
	@Test
	public void bubbleSortWithForWhileLoopTest(){
		
		BubbleSort bubbleSort = new BubbleSort();
		assertArrayEquals(new Integer[] {1, 3, 5, 7, 8}, bubbleSort.bubbleSortByForWhileLoop(Arrays.asList(1,5,7,8,3)).toArray());
	}
	
	@Test
	public void insertSortTest(){
		
		InsertSort insertSort = new InsertSort();
		assertArrayEquals(new Integer[] {1, 3, 5, 7, 8}, insertSort.sort(Arrays.asList(1,5,7,8,3)).toArray());
	}
	
	@Test
	public void quickSortTest(){
		
		QuickSort quickSort = new QuickSort();
		assertArrayEquals(new Integer[] {1, 3, 5, 7, 8}, quickSort.sort(Arrays.asList(1,5,7,8,3)).toArray());
	}
	
	@Test
	public void mergeSortTest(){
		
		MergeSort mergeSort = new MergeSort();
		/*List<Integer> list = mergeSort.sort(Arrays.asList(1,5,7,8,3));
		System.out.println(Arrays.toString(list.toArray()));*/
		assertArrayEquals(new Integer[] {1, 3, 5, 7, 8}, mergeSort.sort(Arrays.asList(1,5,7,8,3,8)).toArray());
	}
	
	@Test
	public void binarySearchTest(){
		
		BinarySearch binarySearch = new BinarySearch();
		Assert.assertEquals(true, binarySearch.search(Arrays.asList(1,5,7,8,3,8), 5));
	}
	
	@Test
	public void fibonniciSeriesTest(){
		Assert.assertEquals(2, FibonniciSeries.getNthElement(3));
		Assert.assertEquals(5, FibonniciSeries.getNthElement(5));
	}
	
	@Test
	public void factorialTest(){
		Assert.assertEquals(1, Factorial.getFactorial(0));
		Assert.assertEquals(6, Factorial.getFactorial(3));
		Assert.assertEquals(120, Factorial.getFactorial(5));
	}
	
	@Test
	public void reverseStringTest(){
		Assert.assertEquals("fedcba", ReverseString.reverseByRecursion("abcdef"));
	}
	
	@Test
	public void linkedListTest(){
		
		LinkedList<String> linkedList = new LinkedList<String>("Sachin", null);
		LinkedList<String> linkedList1 = new LinkedList<String>("rane", linkedList);
		LinkedList<String> linkedList2 = new LinkedList<String>("pune", linkedList1);
		
		System.out.println("given list:");
		(new LinkedList()).traverse(linkedList2);
		
		System.out.println("\n After reverse:");
		LinkedList<String> reversed = (new LinkedList()).reverse(linkedList2);
		new LinkedList().traverse(reversed);
	}
	
	@Test
	public void palindromeTest(){
		Assert.assertEquals(true, new Palindrome().palindromeCheck("@kadak@"));
		Assert.assertEquals(true, new Palindrome().palindromeCheck("a"));
		
	}
	
	@Test
	public void absoluteOfMostNegativeValue() {
	final int mostNegative = Integer.MIN_VALUE;
	final int negated = Math.abs(mostNegative);
	assertFalse(negated>0);
	}
	
	@Test(expected = NullPointerException.class)
	public void expectNullPointerExceptionToBeThrown() {
	final String s = null;
	final int stringLength = s.length();
	}
	
	@Test
	public void primitiveRefCheck() {
		
		int i=20;
		int j=i;
		i=i+1;
		System.out.println("j="+j);
		assertEquals(20, j);
		
	}
	@Test
	public void instanceRefCheck() {
		List<Integer> list = new ArrayList<Integer>();
		list.add(10);
		
		assertEquals(1, list.size());
		
		List<Integer> list1 = list;
		
		list1.add(20);
		
		assertEquals(2, list.size());
		
	}
	
	@Test
	public void stringChanges() {
	final String greeting = "Good Morning, Sachin";
	final String substring = greeting.substring(5);
	assertTrue(substring.equals("Morning, Sachin"));
	assertFalse(greeting.equals(substring));
	assertTrue(greeting.equals("Good Morning, Sachin"));
	}
	
	@Test
	public void genericTypesCheck() {
		ArrayList arraylist = new ArrayList();
		arraylist.add("Sachin");
		arraylist.add(10);
		arraylist.add("Pune");
		assertTrue(arraylist.get(1) instanceof Integer);//reified not in java
		}
	
}
