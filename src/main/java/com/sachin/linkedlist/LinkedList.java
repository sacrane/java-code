package com.sachin.linkedlist;

public class LinkedList<T> {
	T element;;
	LinkedList<T> next;
	
	public LinkedList(){
		
	}
	
	public LinkedList(T element, LinkedList<T> next) {
		this.element = element;
		this.next = next;
	}
	
	public T getElement(){
		return element;
	}
	
	public LinkedList<T> getNext(){
		return next;
	}
	
	public LinkedList<T> reverse( LinkedList<T> list){
		
		if(list == null ) {throw new IllegalStateException("Cant reverse null linked list");}
		
		if(list.next == null){
			return list;
		}
		
		LinkedList<T> temp = list.next;
		list.next=null;
		
		LinkedList<T> reversed = reverse(temp);
		temp.next=list;
		return reversed;
		
	}
	
	public void traverse(LinkedList<T> list){
		while(list != null){
			System.out.print(list.element);
			if(list.next != null){
				System.out.print("->");
			}
			list=list.next;
		}
		
	}
}
