package com.sachin;

public class Palindrome {
	
	public boolean palindromeCheck(String str){
		if(null == str || str.isEmpty()) {
			return false;
		}
		
		int left=0;
		int right = str.length()-1;
		
		while(left <= right){
			
			if(str.charAt(left) != str.charAt(right)){
				return false;
			}
			left++; 
			right--;
			
		}
		
		return true;
	}

}
