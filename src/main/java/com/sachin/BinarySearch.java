package com.sachin;

import java.util.List;

public class BinarySearch {
	
	public boolean search(List<Integer> list, int number){
		if(null != list && list.isEmpty()){
			return false;
		}
		
		int middle = list.size()/2;
		
		if(list.get(middle) == number){
			return true;
		}
		if(list.get(middle) > number){
			return search(list.subList(0, middle), number);
		}else{
			return search(list.subList(middle+1, list.size()), number);
		}
		
		
	}

}
