package com.sachin;

public class ReverseString {

	public static String reverseByRecursion(String str){
		
		if(null != str && str.isEmpty()){
			return str;
		}
		if(str.length() == 0){
			return str;
		}
		
		System.out.println("Track:"+str.substring(1) + "+"+str.charAt(0));
		return reverseByRecursion(str.substring(1))+str.charAt(0);
		
	}
}
