package com.sachin;

import java.util.Arrays;
import java.util.List;

public class BubbleSort {
	
	public List<Integer> bubbleSortTwoForLoop(List<Integer> list){
		
		for(int i=0;i<list.size()-1;i++){
			for(int j=i+1;j<=list.size()-1;j++){
				if(list.get(j)>list.get(i)){
					switchElements(j,i,list);
					
				}
			}
		}
		
		return list;
		
		
	}
	

	public List<Integer> bubbleSortByForWhileLoop(List<Integer> list){
		boolean flag;
		do{
			flag=false;
			for(int i=0;i<list.size()-1;i++){
				if(list.get(i)>list.get(i+1)){
					switchElements(i+1,i,list);
					flag=true;
				}
			}
			
		}while(flag);
		
		return list;
		
		
	}

	private void switchElements(Integer index1, Integer index2, List<Integer> list) {
		
		int temp = list.get(index1);
		list.set(index1, list.get(index2));
		list.set(index2, temp);
		System.out.println("After Swap: "+ Arrays.toString(list.toArray()));
		
	}
}
