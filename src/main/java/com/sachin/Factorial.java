package com.sachin;

public class Factorial {
	
	public static int getFactorial(int number){
		if(number<0){
			throw new IllegalStateException("Negative number not allowed!!");
		}
		if(number == 0){
			System.out.println("track:"+number);
			return 1;
		}
		System.out.println("track:"+number + "* getFactorial("+(number-1)+")");
		return (number * getFactorial(number-1));
	}

}
