package com.sachin;

public class FibonniciSeries {
	//get the nth element of fibonicci series
	
	public static int getNthElement(int n){
		if(n<0){
			throw new IllegalStateException("number should be greater than or equal to  0");
		}
		
		if(n == 0) return 0;
		if (n == 1) return 1;
		
		return (getNthElement(n-1)+getNthElement(n-2));
		
 	}
	
	 static int fib(int n) 
	    { 
	    if (n <= 1) 
	       return n; 
	    return fib(n-1) + fib(n-2); 
	    } 

}
