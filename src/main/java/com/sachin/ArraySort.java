package com.sachin;

import java.util.Arrays;
import java.util.Comparator;

public class ArraySort {
	
	public Integer[] naturalArraySort(Integer[]  array){
		
		Arrays.sort(array);
		return array;
		
	}
	
	public Integer[] customReverseArraySort(Integer[]  array){
		
		Arrays.sort(array, new Comparator<Integer>() {

			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}


		});
		return array;
		
	}

}
