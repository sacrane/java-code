package com.sachin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeSort {
	
	public List<Integer> sort(List<Integer> list){
		System.out.println("merging:"+Arrays.toString(list.toArray()));
		if(list.size()<2){
			return list;
		}
		
		int middle = list.size()/2;
		
		List<Integer> lower = list.subList(0, middle);
		List<Integer> higher = list.subList(middle, list.size());
		
		return merge(sort(lower), sort(higher));
		
		
	}

	private List<Integer> merge(List<Integer> l1, List<Integer> l2) {
		//merging two lists
		int leftPtr=0;
		int rightPtr=0;
		
		List<Integer> merged = new ArrayList<Integer>(l1.size()+l2.size());
		
		while(leftPtr<l1.size() && rightPtr < l2.size()){
			if(l1.get(leftPtr) < l2.get(rightPtr)){
				merged.add(l1.get(leftPtr));
				leftPtr++;
			}else {
				merged.add(l2.get(rightPtr));
				rightPtr++;
			}
		}
		
		while (leftPtr < l1.size()) {
			merged.add(l1.get(leftPtr));
			leftPtr++;
			
		}
		while (rightPtr < l2.size()) {
			merged.add(l2.get(rightPtr));
			rightPtr++;
			
		}
		System.out.println("merged:"+Arrays.toString(merged.toArray()));
		
		return merged;
	}

}
