package com.sachin;

import java.util.List;

public class FindLargestInList {
	
	public int getLargest(List<Integer> list){
			
		int largest=-1;
		
		for (Integer integer : list) {
			if(integer>largest){
				largest=integer;
			}
		}
		
		return largest;
	}

}
