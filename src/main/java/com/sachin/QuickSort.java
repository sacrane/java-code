package com.sachin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuickSort {

	public List<Integer> sort(List<Integer> list){
		
		if (list.size()<2){
			return list;
		}
		
		int pivot = list.get(0);
		List<Integer> lower = new ArrayList<Integer>();
		List<Integer> higher = new ArrayList<Integer>(); 
		int count=0;
		for (Integer integer : list) {
			if(count>0){
			if(pivot>integer){
				lower.add(integer);
			}else {
				higher.add(integer);
			}
			}
			count++;
		}
		
		List<Integer> sortedList = sort(lower);
		sortedList.add(pivot);
		sortedList.addAll(sort(higher));
		System.out.println("Sorted Iteration:"+Arrays.toString(sortedList.toArray()));
		return sortedList;
		
	}

}
